#include "fraction.h"
#include "gauss.h"
#include "matrix.h"

#include <stdio.h>

int main() {
	Matrix_t a;
	matrix_read(&a);

	printf("Initial matrix:\n");
	matrix_print(&a);
	gauss_solve(&a);
	printf("Solved form:\n");
	matrix_print(&a);

	matrix_deinit(&a);

	return 0;
}
