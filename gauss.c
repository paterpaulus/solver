#include "gauss.h"

int find_step_in_col(Matrix_t *matrix, int col);

/*
 * Solves a linear equation system using a variant of the Gaussian algorithm.
 */
void gauss_solve(Matrix_t *m) {
	Fraction_t factor;

	for (int col = 1; col <= m->cols; col++) {
		int ref_row = find_step_in_col(m, col);
		if (ref_row == 0) {
			continue;
		}

		// Make the colth place one
		fraction_set_copy(&factor, matrix_at(m, ref_row, col));
		fraction_inv_mul(&factor);
		matrix_multiply_row(m, ref_row, &factor);

		// Make the colth place of any other row 0
		for (int row = 1; row <= m->rows; row++) {
			if (row == ref_row) {
				continue;
			}

			fraction_set_copy(&factor, matrix_at(m, ref_row, col));
			fraction_multiply(&factor, matrix_at(m, row, col));
			fraction_inv_add(&factor);

			matrix_add_row(m, row, ref_row, &factor);
		}

		// Put row into the right place
		if (col <= m->rows) {
			matrix_swap_rows(m, ref_row, col);
		}
	}

	return;
}

/*
 * Finds the index of a row where the colth place is tcolth place is the first non-zero place.
 * Returns 0 if there is none such row.
 */
int find_step_in_col(Matrix_t *m, int col) {
	for (int row = 1; row <= m->rows; row++) {
		for (int current_col = 1; current_col <= col; current_col++) {
			if (current_col == col) {
				if (!fraction_is_zero(matrix_at(m, row, current_col))) {
					return row;
				} else {
					break;
				}
			}

			if (!fraction_is_zero(matrix_at(m, row, current_col))) {
				break;
			}
		}
	}

	return 0;
}
