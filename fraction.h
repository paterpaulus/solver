#pragma once

typedef struct {
	int denominator;
	int divisor;
} Fraction_t;

void fraction_set(Fraction_t *frac, int denominator, int divisor);
void fraction_read(Fraction_t *frac);
void fraction_set_copy(Fraction_t *frac, Fraction_t *to_copy_from);
void fraction_print(Fraction_t *frac);
void fraction_scale(Fraction_t *frac, int scalar);
void fraction_add(Fraction_t *frac, Fraction_t *to_multiply_with);
void fraction_inv_add(Fraction_t *frac);
void fraction_multiply(Fraction_t *frac, Fraction_t *to_multiply_with);
void fraction_inv_mul(Fraction_t *frac);
int fraction_is_zero(Fraction_t *frac);
