#include <stdio.h> // fprintf, stderr
#include <stdlib.h> // exit

#include "fraction.h"

int euclidean_gcd(int a, int b);

/*
 * Sets a fractions denominator and divisor.
 */
void fraction_set(Fraction_t *frac, int denominator, int divisor) {
	if (divisor == 0) {
		fprintf(stderr, "Divisor can never be 0!\n");
		exit(1);

		return;
	}

	if (denominator == 0) {
		frac->denominator = 0;
		frac->divisor = 1;
	}

	int gcd = euclidean_gcd(denominator, divisor);

	frac->denominator = denominator / gcd;
	frac->divisor = divisor / gcd;

	if (frac->divisor < 0) {
		frac->divisor *= -1;
		frac->denominator *= -1;
	}

	return;
}

/*
 * Reads a fraction into frac from stdin.
 * The input format is either a single integer or two integers separated by a slash.
 * If the end of file is reached or no number can be read, terminate the program.
 * TODO: on error, do not terminate, but return some sort of status code
 */
void fraction_read(Fraction_t *frac) {
	// IDEA: if I set divisor to 1 here, do i still need block (1)?
	// NOTE: look up scanf behaviour when it doesnt find a match
	int denominator;
	int divisor;

	// the leading space ignores any incoming whitespace
	int numbers_read = scanf(" %d/%d", &denominator, &divisor);

	if (
		numbers_read == EOF
		|| !(numbers_read == 1 || numbers_read == 2)
	) {
		fprintf(stderr, "Input expected: integer or fraction (${DENOM}/${DIVI})!\n");
		exit(1);
	}

	// (1)
	if (numbers_read == 1) {
		divisor = 1;
	}

	fraction_set(frac, denominator, divisor);
}

/*
 * Copies a fraction into another fraction.
 */
void fraction_set_copy(Fraction_t *frac, Fraction_t *to_copy_from) {
	fraction_set(
		frac,
		to_copy_from->denominator,
		to_copy_from->divisor
	);
}

/*
 * Adds a fraction to another.
 */
void fraction_add(Fraction_t *frac, Fraction_t *to_add) {
	fraction_set(
		frac,
		frac->denominator * to_add->divisor + to_add->denominator * frac->divisor,
		frac->divisor * to_add->divisor
	);
}

/*
 * Makes frac its additive inverse (multiples with -1).
 */
void fraction_inv_add(Fraction_t *frac) {
	frac->denominator *= -1;
}

/*
 * Multiplies frac with to_multiply in-place.
 */
void fraction_multiply(Fraction_t *frac, Fraction_t *to_multiply) {
	fraction_set(
		frac,
		frac->denominator * to_multiply->denominator,
		frac->divisor * to_multiply->divisor
	);
}

/*
 * Makes frac its multiplicative inverse (swaps denom and divi).
 */
void fraction_inv_mul(Fraction_t *frac) {
	fraction_set(frac, frac->divisor, frac->denominator);
}

/*
 * Prints a fraction to stdout.
 */
void fraction_print(Fraction_t *frac) {
	if (frac->divisor == 1) {
		printf("%d", frac->denominator);
	} else {
		printf("%d/%d", frac->denominator, frac->divisor);
	}
}

int fraction_is_zero(Fraction_t *frac) {
	return frac->denominator == 0;
}

void fraction_scale(Fraction_t *frac, int scalar) {
	frac->denominator *= scalar;
}

// INTERNAL

/*
 * Calculates the greatest common divisor of two numbers a and b using Euclids algorithm.
 * a may never be 0.
 */
int euclidean_gcd(int a, int b) {
	if (b == 0) {
		return a;
	}

	return euclidean_gcd(b, a % b);
}
