#pragma once

#include "fraction.h"

typedef struct {
	int rows;
	int cols;
	Fraction_t *cells;
} Matrix_t;

void matrix_init(Matrix_t *m, int rows, int cols);
void matrix_read(Matrix_t *m);
void matrix_deinit(Matrix_t *m);
Fraction_t *matrix_at(Matrix_t *m, int row, int col);
void matrix_print(Matrix_t *m);
void matrix_add_row(Matrix_t *m, int to_row, int from_row, Fraction_t *factor);
void matrix_multiply_row(Matrix_t *m, int row, Fraction_t *factor);
void matrix_swap_rows(Matrix_t *m, int a, int b);
