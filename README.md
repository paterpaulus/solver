# solver

A C program that solves linear equation systems of `int`-fractions, using a variant of the Gaussian algorithm.

By no means done or anywhere close to bugfree.

## Usage

The `solver` executable reads a matrix from stdin, using the following format:

```ROWSxCOLS A1,1 A1,2 A1,3 ...```

Where `ROWS` and `COLS` are integers and `Ar,c` is the matrix entry at row `r` and column `c`.
Entries can be integers or fractions (written as `denominator/divisor`).
A space represents any amount of whitespace (including tabs, newlines, etc.).

Example of usage:

```
[paul@thinkmuffin solver]$ cat examples/readme.mat 
3x3
1 2 3
4/7 5/7 6/7
7 8 9
[paul@thinkmuffin solver]$ cat examples/readme.mat | ./solver 
Initial matrix:
3x3
1 2 3
4/7 5/7 6/7
7 8 9
Solved form:
3x3
1 0 -1
0 1 2
0 0 0
```

## To Do

- Tests for input checks
- `prettify` program for printing pretty matrices
- Make `matrix->{rows, cols}` unsigned
- Other algorithms (e.g. finding all solutions for a system that does not have a single solution)
- Maybe write some tests

## Done

- Input reading: I want to be able to do `echo "3x3 1 2 3 4 5 6 7 8 9" | ./solver`
- Input sanitation/checks **done**
    - `fraction_read` function **done**
