solver: solver.c matrix.h matrix.o gauss.h gauss.o fraction.h fraction.o
	gcc -o solver solver.c matrix.o gauss.o fraction.o

matrix.o: matrix.h matrix.c fraction.h
	gcc -c matrix.c

gauss.o: gauss.h gauss.c matrix.h fraction.h
	gcc -c gauss.c

fraction.o: fraction.h fraction.c
	gcc -c fraction.c
