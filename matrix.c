#include <stdio.h> // printf
#include <stdlib.h> // calloc, free

#include "matrix.h"

/*
 * Initializes a matrix (allocates memory for cells and such).
 */
void matrix_init(Matrix_t *new_matrix, int rows, int cols) {
	new_matrix->rows = rows;
	new_matrix->cols = cols;
	new_matrix->cells = calloc(sizeof(Fraction_t), rows * cols);
}

/*
 * Deinitializes a matrix (frees memory occupied by its cells).
 */
void matrix_deinit(Matrix_t *m) {
	free(m->cells);
}

/*
 * Adds row from_row multiplied with factor to row to_row.
 */
void matrix_add_row(Matrix_t *m, int to_row, int from_row, Fraction_t *factor) {
	Fraction_t multiplied;

	for (int col = 1; col <= m->cols; col++) {
		fraction_set_copy(&multiplied, matrix_at(m, from_row, col));
		fraction_multiply(&multiplied, factor);
		fraction_add(matrix_at(m, to_row, col), &multiplied);
	}
}

/*
 * Multiples row row with factor.
 */
void matrix_multiply_row(Matrix_t *m, int row, Fraction_t *factor) {
	for (int col = 1; col <= m->cols; col++) {
		fraction_multiply(matrix_at(m, row, col), factor);
	}
}

/*
 * Frees the memory used by a matrix and also frees each of its cells.
 */
void matrix_destroy(Matrix_t *m) {
	free(m->cells);
	free(m);
}

/*
 * Gets an entry of the matrix.
 * WARNING: indices start at 1!
 */
Fraction_t *matrix_at(Matrix_t *m, int row, int col) {
	int index = (row - 1) * m->cols + (col - 1);

	return m->cells + index;
}

/*
 * Swaps rows a and b.
 * Does not actually swap when a is the same as b.
 * Kills the process if one of them is greater than m->rows.
 */
void matrix_swap_rows(Matrix_t *m, int a, int b) {
	if (a == b) {
		return;
	}

	if (a > m->rows || b > m->rows) {
		fprintf(stderr, "Can not swap rows %d and %d, this matrix has only %d rows!\n", a, b, m->rows);
		exit(1);
	}

	// yep, should exchange ptrs here
	// but that calls for matrix_set_at
	for (int col = 1; col <= m->cols; col++) {
		Fraction_t temp;

		fraction_set_copy(&temp, matrix_at(m, a, col));
		fraction_set_copy(matrix_at(m, a, col), matrix_at(m, b, col));
		fraction_set_copy(matrix_at(m, b, col), &temp);
	}
}

/*
 * Initializes a matrix by reading the following format from stdin:
 * Format: RxC A1,1 A1,2 A1,3 ...
 * More on the format in the comment on matrix_print and the readme.
 */
void matrix_read(Matrix_t *a) {
	int rows;
	int cols;

	int input_status = scanf("%dx%d", &rows, &cols);

	if (input_status == EOF || input_status != 2) {
		fprintf(stderr, "Input expected: size of matrix ROWSxCOLS!\n");
		exit(1);
	}

	matrix_init(a, rows, cols);

	int current_denom;
	int current_divi;

	for (int row = 1; row <= rows; row++) {
		for (int col = 1; col <= cols; col++) {
			fraction_read(matrix_at(a, row, col));
		}
	}
}

/*
 * Prints a matrix to stdout.
 * The first line is of the form ${ROWS}x${COLUMNS}.
 * The following ${ROWS} lines each contain a ${COLUMNS} fr${COLUMNS} fractions, seperated by spaces.
 */
void matrix_print(Matrix_t *m) {
	// print dimensions
	printf("%dx%d\n", m->rows, m->cols);

	// print each line
	for (int row = 1; row <= m->rows; row++) {
		for (int col = 1; col <= m->cols; col++) {
			fraction_print(matrix_at(m, row, col));

			if (col == m->cols) {
				printf("\n");
			} else {
				printf(" ");
			}
		}
	}
}
